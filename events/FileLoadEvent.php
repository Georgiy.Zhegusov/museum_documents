<?
namespace app\events;

use app\models\Museum;
use yii\base\Event;

class FileLoadEvent extends Event
{
	public $status;

	/**
	 * FileUploadEvent constructor.
	 * @param Status $status
	 */
	/** @noinspection PhpMissingParentConstructorInspection */
	public function __construct(Museum $status)
	{
		$this->status = $status;
	}
}