<?php

use yii\helpers\Html;
use app\models\Event;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'События';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить событие', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'triggeredEvent',
	        [
		        'attribute' => 'updated_at',
		        'format' => ['date', 'php:Y-m-d H:i:s']
	        ],
	        [
		        'attribute' => 'created_at',
		        'format' => ['date', 'php:Y-m-d H:i:s']
	        ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
