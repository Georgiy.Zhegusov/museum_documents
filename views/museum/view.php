<?php

use app\models\search\FileSearch;
use yii\helpers\Html;
use app\modules\logger\loggerWidget;
use kartik\tabs\TabsX;
use yii\helpers\Url;
use yii2mod\comments\widgets\Comment;
use app\models\User;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Museum */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Музеи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<?php
	$tabs = [];
	$tabs[] = [
		'label'=>'<i class="glyphicon glyphicon-info-sign"></i> О музее',
		'content'=> $this->render('_info',['model'=>$model]),
		//'linkOptions'=>['data-url'=>Url::to(['/site/fetch-tab?tab=2'])]
		'active'=>true,
	];
	if(Yii::$app->user->can('museumLog')) {
		$tabs[] = [
			'label' => '<i class="glyphicon glyphicon-list"></i> Логи',
			'content' => loggerWidget::widget([
				'object' => $model,
				'userId' => Yii::$app->user->id,
			]),
			//'linkOptions'=>['data-url'=>Url::to(['/site/fetch-tab?tab=1'])]
		];
	}
	if(Yii::$app->user->can('museumHistory')) {
		$tabs[] = [
			'label' => '<i class="glyphicon glyphicon-piggy-bank"></i> История статусов',
			'content' => $this->render(
				'//status-history/_museum-history',
				[
					'dataProvider'=> new ActiveDataProvider([
						'query' => $model->getMStatuses()->orderBy('created_at DESC')]),
					'users'=>User::find()->indexBy('id')->all()
				])];
	}
	if(Yii::$app->user->can('fileIndex')) {
		$searchModel = new FileSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $model->id);
		$tabs[] = [
			'label' => '<i class="glyphicon glyphicon-file"></i> Файлы',
			'content' => $this->renderAjax('//file/index_for_museum', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			])];
	}
	$tabs[] = [
		'label'=>'<i class="glyphicon glyphicon-comment"></i> Комментарии',
		'content'=> Comment::widget([
			'model' => $model,
			'relatedTo' => 'User ' . \Yii::$app->user->identity->fullname . ' commented on the page ' . Url::current(), // for example
			'perPage' => null
		]),
		//'linkOptions'=>['data-url'=>Url::to(['/site/fetch-tab?tab=1'])]
	];
	?>

	<?= TabsX::widget([
		'items'=> $tabs,
		'position'=>TabsX::POS_ABOVE,
		'encodeLabels'=>false
	]);?>

</div>
