<?php

use app\models\Museum;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\models\File;
use yii2mod\comments\widgets\LastComment;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MuseumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Музеи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php if(Yii::$app->user->can('museumCreate')):?>
		<p>
			<?= Html::a('Добавить музей', ['create'], ['class' => 'btn btn-success']) ?>
		</p>
	<?php endif;?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => 'Отсутствует'],
		'columns' => [
			//['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'attribute' => 'name',
				'label' => 'Название',
				'format' => 'raw',
				'value' => function(Museum $museum) {
					return Html::a($museum->name, Url::to(['museum/view', 'id' => $museum->id]),['target'=>'_blank']);
				},
			],
			[
				'attribute' => 'lastFileName',
				'label' => 'Крайний файл',
				'format' => 'raw',
				'value' => function(Museum $museum) {
					if(is_null($museum->lastFile)){
						return 'Файл отсутствует';
					}
					if(!Yii::$app->user->can('fileDownload',[
						'museum' => $museum,
						'file' => $museum->lastFile,
					])){
						return 'Скачивание заблокировано.';
					}
					if ($museum->lastFile){
						return $this->render('//file/_link', [
							'model' => $museum->lastFile,
						]);
					}
					return null;
				},
			],
			[
				'label' => 'Последний комментарий',
				'format' =>'raw',
				'value' => function(Museum $museum){
					return LastComment::widget([
						'model' => $museum,
						'relatedTo' => 'User ' . \Yii::$app->user->identity->fullname . ' commented on the page ' . Url::current(), // for example
						'perPage' => null
					]);
				},
				//'contentOptions'=>['style'=>'width: 300px;'],
			],
//            [
//                'attribute' => 'statusName',
//                'label' => 'Статус',
//                'format' => 'text', // Возможные варианты: raw, html
//                'value' => 'status.name',
//            ],
			[
				'attribute' => 'loadFileName',
				'label' => 'Загрузить файл',
				'format' => 'raw', // Возможные варианты: raw, html
				'value' => function(Museum $museum){
					if(!Yii::$app->user->can('fileCreate',[
						'museum' => $museum,
					])){
						return 'Загрузка заблокирована.';
					}
					$file = new File();
					$file->loadMuseum($museum);
					return $this->render('//file/_form_for_museum',['model'=>$file]);
				}
			],
		],
	]); ?>
</div>

