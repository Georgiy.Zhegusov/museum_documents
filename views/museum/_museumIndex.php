<?php

use app\models\Museum;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $museums yii\data\ActiveDataProvider */
/** @var array $statuses */
?>

<?= GridView::widget([
	'dataProvider' => $museums,
	'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => 'Отсутствует'],
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],
		[
			'attribute' => 'id',
			'contentOptions'=>['style'=>'width: 60px;'],
		],
		'name',
//        [
//            'attribute' => 'statusName',
//            'label' => 'Статус',
//            'format' => 'text', // Возможные варианты: raw, html
//            'value' => 'status.name',
//        ],
//		[
//			'attribute' => 'lastFileName',
//			'label' => 'Файлы',
//			'format' => 'raw',
//			'value' => function(Museum $museum) {
//				$out = [];
//				if ($museum->lastFile) {
//					$out[] = 'Крайний файл:'.$this->render('//file/_link', [
//							'model' => $museum->lastFile,
//						]);
//				}
//				$out[] = Html::a('Загрузить',Url::to(['file/create_for_museum', 'id' => $museum->id]));
//				$out[] = Html::a('ПросмотретьВсе',Url::to(['file/index_for_museum', 'id' => $museum->id]));
//				return implode("\n", $out);
//			},
//		],
	],
]); ?>