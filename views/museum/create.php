<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Museum */
/* @var array $statuses */

$this->title = 'Добавить музеи';
$this->params['breadcrumbs'][] = ['label' => 'Музеи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= isset($error) ? $error : 'Ошибок нет'?>
    <?= $this->render('_form', [
        'model' => $model,
        'statuses' => $statuses,
	    'file' => $file,
    ]) ?>

</div>
