<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Museum */

$this->title = 'Изменить музей ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Музеи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="museum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'statuses' => $statuses,
    ]) ?>

</div>
