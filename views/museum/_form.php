<?php

use app\models\Status;
use app\models\User;
use app\models\File;
use app\models\Museum;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Museum */
/* @var $form yii\widgets\ActiveForm */
/* @var $statuses Status[]*/
$file = new File;
?>

<div class="museum-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'statusId')->dropDownList(ArrayHelper::getColumn( $statuses,'name')) ?>

    <?= $form->field($model, 'additional')->dropDownList(Museum::getAdditionalList(), ['prompt' => 'Ничего особенного']) ?>

    <?= $form->field($model, 'expertId')->dropDownList([null=>'Пусто']+ArrayHelper::getColumn( User::find()->indexBy('id')->all(),'fullname')) ?>

	<?= $form->field($model, 'contacts')->textarea(['rows' => 5]) ?>

	<hr>

	<?if (isset($file)):?>

		Загрузите файл, али ссылку на ресурс заморский, ежели есть:

	<?=	$this->render('//file/_filePoles',
			[
				'form' => $form,
				'model' => $file,
			]);?>
	<?endif;?>

	<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
