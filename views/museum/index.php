<?php

use app\models\Museum;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\File;
use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Collapse;
use \yii2mod\comments\widgets\LastComment;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MuseumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var array $statuses */



$this->title = 'Музеи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить музей', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
	    'floatHeader'=>true,
	    'floatHeaderOptions'=>['position' => 'auto','top'=>50],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
	    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => 'Отсутствует'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
	            'attribute' => 'id',
	            'contentOptions'=>['style'=>'width: 60px;'],
            ],
	        [
		        'attribute' => 'name',
		        'label' => 'Название',
		        'format' => 'raw',
		        'value' => function(Museum $museum) {
			        return Html::a($museum->name, Url::to(['museum/view', 'id' => $museum->id]),['target'=>'_blank']);
		        },
	        ],
//            [
//                'attribute' => 'statusName',
//                'label' => 'Статус',
//                'format' => 'text', // Возможные варианты: raw, html
//                'value' => 'status.name',
//            ],
	        [
		        'attribute' => 'statusName',
		        'label' => 'Статус',
		        'format' => 'raw', // Возможные варианты: raw, html
		        'value' => function(Museum $museum) use ($statuses){
			        return Editable::widget([
				        'id' => $museum->id . 'editableWidget',
				        'formOptions' => [
					        'action' => Url::to(['museum/ajax-update']),
				        ],
				        'model' => $museum,
				        'displayValue' => $museum->statusName,
				        'attribute' => 'statusId',
				        'name'=>'Статус',
				        'asPopover' => true,
				        'header' => 'Статус',
				        'format' => Editable::FORMAT_BUTTON,
				        'inputType' => Editable::INPUT_DROPDOWN_LIST,
				        'data'=> ArrayHelper::getColumn($statuses, 'name'),
				        'options' => ['class'=>'form-control', 'prompt'=>'Выберете статус', 'id'=>uniqid('museumStatus')],
				        'beforeInput' => Html::hiddenInput('Museum[id]', $museum->id ),
			        ]);
		        }
	        ],
            [
		        'attribute' => 'additionalName',
		        'label' => 'Доп. инфо',
                'filter' => Museum::getAdditionalList()
	        ],
	        [
		        'attribute' => 'expertFullname',
		        'label' => 'Эксперт',
		        'format' => 'text', // Возможные варианты: raw, html
		        'value' => 'expert.fullname',
	        ],
	        [
		        'attribute' => 'lastFileName',
		        'label' => 'Файлы',
		        'format' => 'raw',
		        'value' => function(Museum $museum) {
			        $out = [];
			        if ($museum->lastFile) {
				        $out[] = 'Крайний файл:'.$this->render('//file/_link', [
					        'model' => $museum->lastFile,
				        ]);
		            }
			        $out[] = Html::a('Загрузить',Url::to(['file/create_for_museum', 'id' => $museum->id]),['target'=>'_blank']);
			        $out[] = Html::a('ПросмотретьВсе',Url::to(['file/index_for_museum', 'id' => $museum->id]),['target'=>'_blank']);
			        return implode("\n", $out);
		        },
	        ],
	        [
		        'label' => 'Последний комментарий',
		        'format' =>'raw',
		        'value' => function(Museum $museum){
			        return LastComment::widget([
				        'model' => $museum,
				        'relatedTo' => 'User ' . \Yii::$app->user->identity->fullname . ' commented on the page ' . Url::current(), // for example
				        'perPage' => null
			        ]);
		        },
		        //'contentOptions'=>['style'=>'width: 300px;'],
	        ],
	        [
		        'attribute' => 'updated_at',
		        'format' => ['date', 'php:Y-m-d H:i:s']
	        ],
//	        [
//		        'attribute' => 'created_at',
//		        'format' => ['date', 'php:Y-m-d H:i:s']
//	        ],
            ['class' => 'yii\grid\ActionColumn',],
			[
				'attribute' => 'contacts',
				'format' => 'ntext'
			],
        ],
    ]); ?>
</div>
