<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $model \app\models\Museum */
?>

<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'name',
			[
				'attribute' => 'statusId',
				'value' => $model->statusName,
			],
			[
				'attribute' => 'expertId',
				'value' => $model->expert?$model->expert->fullName:null,
			],
			[
				'attribute' => 'contacts',
				'format' => 'ntext'
			],
            [
				'attribute' => 'additionalName',
                'format' => 'ntext'
			],
			[
				'attribute' => 'updated_at',
				'format' => ['date', 'php:Y-m-d H:i:s']
			],
			[
				'attribute' => 'created_at',
				'format' => ['date', 'php:Y-m-d H:i:s']
			],
		],
	]);?>

<p>
	<?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	<?= Html::a('Удалить', ['delete', 'id' => $model->id], [
		'class' => 'btn btn-danger',
		'data' => [
			'confirm' => 'Вы уверены, что хотите удалить музей ' . $model->name . '?',
			'method' => 'post',
		],
	]) ?>
</p>
