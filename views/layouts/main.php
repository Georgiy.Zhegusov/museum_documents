<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use \yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Музеи.Парки.Усадьбы',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'fixed-height navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Музеи', 'url' => ['/museum/index'], 'visible' => Yii::$app->user->can('museumNav')],
            ['label' => 'Аналитика', 'url' => ['/site/status-line'], 'visible' => Yii::$app->user->can('statusLineNav')],
            ['label' => 'Файлы', 'url' => ['/file/index'], 'visible' => Yii::$app->user->can('fileNav')],
	        [
		        'label' => 'Все про статусы',
		        'items' => [
		            ['label' => 'Статусы', 'url' => ['/status/index'], 'visible' => Yii::$app->user->can('statusNav')],
		            ['label' => 'Авто-статусы', 'url' => ['/auto-status/index'], 'visible' => Yii::$app->user->can('autoStatusNav')],
		            ['label' => 'События', 'url' => ['/event/index'], 'visible' => Yii::$app->user->can('eventNav')],
			    ],
		        'visible' =>
			        Yii::$app->user->can('statusNav') ||
			        Yii::$app->user->can('autoStatusNav') ||
			        Yii::$app->user->can('eventNav'),
	        ],
	        [
		        'label' => 'Все про людей',
		        'items' => [
			        ['label' => 'Про меня', 'url' => ['/user/view','id'=>Yii::$app->user->id], 'visible' => Yii::$app->user->can('userView',['user'=>Yii::$app->user])],
		            ['label' => 'Про всех', 'url' => ['/user/index'], 'visible' => Yii::$app->user->can('userNav')],
		            ['label' => 'Группы', 'url' => ['/group/index'], 'visible' => Yii::$app->user->can('groupNav')],
		        ],
		        'visible' =>
			        Yii::$app->user->can('userView',['user'=>Yii::$app->user]) ||
			        Yii::$app->user->can('userNav') ||
			        Yii::$app->user->can('groupNav'),
	        ],
            [
		        'label' => 'Логи',
                'url' => ['/logger/logger'],
		        'visible' => Yii::$app->user->can('museumLog'),
	        ],
            Yii::$app->user->isGuest ? (
                ['label' => 'Войти', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(Url::to(['/site/logout']), 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Выйти (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        Для сортировки по столбцу, необходимо кликнуть по его названию.
	    Для фильтрации по столбцу необходимо ввести значение для фильтрации в строку после заголовка.
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Музеи.Парки.Усадьбы <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
