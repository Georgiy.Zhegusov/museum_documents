<?php
	use yii\widgets\DetailView;

	/* @var $model \app\models\User*/
?>

<?= DetailView::widget([
	'model' => $model,
	'attributes' => [
		'id',
		'name',
		'surname',
		'groupName',
		'email:email',
		'phone',
		[
			'attribute' => 'updated_at',
			'format' => ['date', 'php:Y-m-d H:i:s']
		],
		[
			'attribute' => 'created_at',
			'format' => ['date', 'php:Y-m-d H:i:s']
		],
	],
]) ?>