<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\logger\loggerWidget;
use app\models\Museum;
use kartik\tabs\TabsX;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $valuedMuseums Museum[] */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить пользователя ' . $model->fullname . '?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

	<?php
	$tabs = [];

	$tabs[] = [
		'label'=>'<i class="glyphicon glyphicon-info-sign"></i> О пользователе',
		'content'=> $this->render('_info',['model'=>$model]),
		//'linkOptions'=>['data-url'=>Url::to(['/site/fetch-tab?tab=2'])]
		'active'=>true,
	];

	$tabs[] = [
		'label' => '<i class="glyphicon glyphicon-ruble"></i> Проверенные музеи (' . $valuedMuseums->count() . ')',
		'content' => $this->render(
			'//museum/_museumIndex',
			[
				'museums'=> new ActiveDataProvider([
					'query' => $valuedMuseums])
			])
	];
	$tabs[] = [
		'label' => '<i class="glyphicon glyphicon-list"></i> Логи',
		'content' => loggerWidget::widget([
			'object' => $model,
			'userId' => $model->id,
			'byUserOnly' => true,
		])
	];
	?>

	<?= TabsX::widget([
		'items'=> $tabs,
		'position'=>TabsX::POS_ABOVE,
		'encodeLabels'=>false
	]);?>

</div>
