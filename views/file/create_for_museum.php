<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\File */

$this->title = 'Добавить файл для ' . $model->museum->name;
$this->params['breadcrumbs'][] = ['label' => 'Файлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_for_museum', [
        'model' => $model,
    ]) ?>

</div>
