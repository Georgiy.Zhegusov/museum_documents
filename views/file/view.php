<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\File;

/* @var $this yii\web\View */
/* @var $model app\models\File */

$this->title = $model->filename;
$this->params['breadcrumbs'][] = ['label' => 'Файлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if(!$model->isLast):?>
	        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
	            'class' => 'btn btn-danger',
	            'data' => [
	                'confirm' => 'Вы уверены, что хотите удалить файл ' . $model->filename . '?',
	                'method' => 'post',
	            ],
	        ]) ?>
	    <?php endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
	        [
		        'attribute' => 'filename',
		        'format' => 'raw',
		        'value' => $this->render('_link', [
				        'model' => $model,
			        ]),
	        ],
            'uploadedExpertName',
            'museumName',
	        [
		        'attribute' => 'updated_at',
		        'format' => ['date', 'php:Y-m-d H:i:s']
	        ],
	        [
		        'attribute' => 'created_at',
		        'format' => ['date', 'php:Y-m-d H:i:s']
	        ],
        ],
    ]) ?>

</div>
