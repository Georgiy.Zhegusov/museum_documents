<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\File;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Файлы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить файл', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
	    'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'filename',
	            'format' => 'raw',
                'value' => function(File $file){
			        return $this->render('_link', [
				        'model' => $file,
			        ]);
                },
            ],
	        [
		        'attribute'=>'museumName',
		        'label'=>'Музей',
		        'format'=>'text', // Возможные варианты: raw, html
		        'value'=>'museum.name',
	        ],
	        [
		        'attribute'=>'uploadedExpertName',
		        'label'=>'Эксперт',
		        'format'=>'text', // Возможные варианты: raw, html
		        'value'=>'uploadedExpert.fullname',
	        ],
	        [
		        'attribute' => 'updated_at',
		        'format' => ['date', 'php:Y-m-d H:i:s']
	        ],
	        [
		        'attribute' => 'created_at',
		        'format' => ['date', 'php:Y-m-d H:i:s']
	        ],
            [
	            'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ]
        ],
    ]); ?>
</div>
