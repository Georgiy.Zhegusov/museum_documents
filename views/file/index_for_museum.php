<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\File;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="file-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
	    'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'filename',
	            'format' => 'raw',
                'value' => function(File $file){
			        return $this->render('_link', [
				        'model' => $file,
			        ]);
                },
            ],
	        [
		        'attribute'=>'uploadedExpertName',
		        'label'=>'Эксперт',
		        'format'=>'text', // Возможные варианты: raw, html
		        'value'=>'uploadedExpert.fullname',
	        ],
        ],
    ]); ?>
</div>
