<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\File */
?>

<div class="file-link">

	<?=	(!is_null($model->realPath)?
		$model->filename . ' ' .
			Html::a(Html::encode('Скачать'), Url::to(['file/download', 'id' => $model->id]),['target'=>'_blank']):
		Html::a(Html::encode($model->filename), $model->url,['target'=>'_blank'])
	)?>

</div>
