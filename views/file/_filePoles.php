<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Museum;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

	<?= $form->field($model, 'filePole')->fileInput() ?>

	<?= $form->field($model, 'url')->textInput() ?>

</div>
