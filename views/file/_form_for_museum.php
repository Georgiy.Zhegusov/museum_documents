<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Museum;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action'=>Url::to(['file/create_for_museum','id'=>$model->museum->id])]); ?>

	<?= $form->field($model, 'filePole')->fileInput() ?>

	<?= $form->field($model, 'url')->textInput() ?>

	<?= $form->field($model, 'museumId')->hiddenInput()->label(false)?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
