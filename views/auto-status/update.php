<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\autoStatus */
/* @var $statuses array[app\models\Status] */
/* @var $events array[app\models\Events] */

$this->title = 'Изменить автостатус ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Авто-статусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="auto-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'statuses' => $statuses,
	    'events' => $events,
    ]) ?>

</div>
