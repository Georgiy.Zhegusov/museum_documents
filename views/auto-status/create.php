<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\autoStatus */
/* @var $statuses array[app\models\Status] */
/* @var $events array[app\models\Events] */

$this->title = 'Добавить авто-статус';
$this->params['breadcrumbs'][] = ['label' => 'Авто-статусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auto-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'statuses' => $statuses,
	    'events' => $events,
    ]) ?>

</div>
