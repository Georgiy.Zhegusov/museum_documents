<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\autoStatus */
/* @var $statuses array[app\models\Status] */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auto-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'initStatusId')->dropDownList(ArrayHelper::getColumn($statuses, 'name')) ?>

    <?= $form->field($model, 'initEventId')->dropDownList(ArrayHelper::getColumn($events, 'name')) ?>

    <?= $form->field($model, 'triggeredStatusId')->dropDownList(ArrayHelper::getColumn($statuses, 'name')) ?>

    <?= $form->field($model, 'isActive')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
