<?php

use yii\grid\GridView;
use app\models\MuseumStatus;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $users \app\models\User[]*/
?>
<div class="museum-history-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'status.name',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d H:i:s']
            ],
            [
                'label' => 'Добавил',
                'format' =>'raw',
                'value' => function(MuseumStatus $museumStatus) use ($users) {
                    return $users[$museumStatus->created_by]->fullname;
                }
            ]
        ]
    ]); ?>
</div>
