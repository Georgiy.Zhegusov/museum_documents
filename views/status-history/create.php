<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MuseumStatus */

$this->title = 'Create Museum Status';
$this->params['breadcrumbs'][] = ['label' => 'Museum Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
