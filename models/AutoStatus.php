<?php

namespace app\models;

use app\models\Event;
use yii;
use app\models\Status;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "AutoStatuses".

 * @property integer $id
 * @property integer $initStatusId
 * @property integer $initEventId
 * @property integer $triggeredStatusId
 * @property integer $isActive
 *
 * @property Event $initEvent
 * @property Status $initStatus
 * @property Status $triggeredStatus
 */
class AutoStatus extends \yii\db\ActiveRecord
{

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'autoStatuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['initStatusId', 'initEventId', 'triggeredStatusId', 'isActive'], 'integer'],
            [['initEventId', 'triggeredStatusId'], 'required'],
            [['initEventId'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['initEventId' => 'id']],
            [['initStatusId'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['initStatusId' => 'id']],
            [['triggeredStatusId'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['triggeredStatusId' => 'id']],
	        [['initStatusId', 'initEventId','triggeredStatusId'],'filter','filter'=>function($value){return $value==''?null:intval($value);}]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'initStatusId' => 'Инциирующий статус',
            'initStatusName' => 'Инциирующий статус',
            'initEventId' => 'Инициирующее действие',
            'initEventName' => 'Инициирующее действие',
            'triggeredStatusId' => 'Устанавливаемый статус',
            'triggeredStatusName' => 'Устанавливаемый статус',
            'isActive' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInitEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'initEventId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInitStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'initStatusId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTriggeredStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'triggeredStatusId']);
    }

	/**
	 * @return string
	 */
	public function getInitEventName()
	{
		return $this->initEvent->name;
	}

	/**
	 * @return string
	 */
	public function getInitStatusName()
	{
		return $this->initStatus->name;
	}

	/**
	 * @return string
	 */
	public function getTriggeredStatusName()
	{
		return $this->triggeredStatus->name;
	}

	public static function setTriggeredStatus($eventName, Museum $museum){
		$autoStatus = self::find()
			->joinWith('initEvent')
			->filterWhere(['initStatusId' => $museum->statusId])
			->andFilterWhere(['isActive' => true])
			->andFilterWhere([Event::tableName().'.triggeredEvent' => $eventName])
			->limit(1)->one();
		if (!is_null($autoStatus)) {
			$museum->statusId = $autoStatus->triggeredStatusId;
			$museum->save();
		}
	}
}
