<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Status;
use app\models\User;
use app\models\Museum;
use app\models\File;
use yii\data\Sort;
use yii\db\Expression;

/**
 * EventSearch represents the model behind the search form about `app\models\Event`.
 */
class MuseumSearch extends Museum
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	        [['id'], 'integer', 'message'=>'Поле Уровень доступа должно быть целым числом'],
	        [['additionalName'], 'integer', 'message'=>'Поле Доп. инфо должно быть целым числом'],
	        [['name', 'statusName', 'expertFullname', 'comment', 'lastFileName'], 'safe'],
	        [['name'], 'string', 'max' => 256, 'message'=>'Длинна поля {attribute} ограничена 256 символами'],
	        [['contacts'], 'string', 'max' => 512, 'message'=>'Длинна поля {attribute} ограничена 256 символами'],
        ];
    }

	public $statusName;
	public $additionalName;
	public $expertFullname;
	public $lastFileName;

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'statusName' => 'Статус',
			'expertFullname' => 'Эксперт',
			'lastFileName' => 'Крайний файл',
		];
	}

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Museum::find();
	    //$query->orderBy(['id'=>SORT_ASC]);
	    $query->joinWith(['status']);
	    $query->joinWith(['expert']);
	    $query->joinWith(['lastFile']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	        'sort'=> ['defaultOrder' => ['id'=>SORT_ASC]]
        ]);

	    $dataProvider->sort->attributes['statusName'] = [
		    // The tables are the ones our relation are configured to
		    // in my case they are prefixed with "tbl_"
		    'asc' => [Status::tableName().'.name' => SORT_ASC],
		    'desc' => [Status::tableName().'.name' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['lastFileName'] = [
		    // The tables are the ones our relation are configured to
		    // in my case they are prefixed with "tbl_"
		    'asc' => [File::tableName().'.filename' => SORT_ASC],
		    'desc' => [File::tableName().'.filename' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['expertFullname'] = [
		    // The tables are the ones our relation are configured to
		    // in my case they are prefixed with "tbl_"
		    'asc' => [User::tableName().'.surname' => SORT_ASC, User::tableName().'.name' => SORT_ASC],
		    'desc' => [User::tableName().'.surname' => SORT_DESC, User::tableName().'.name' => SORT_DESC],
	    ];

	    if (!($this->load($params) && $this->validate())) {
		    return $dataProvider;
	    }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['additional' => $this->additionalName]);
        $query->andFilterWhere(['like', self::tableName() . '.name', $this->name]);
	    $query->andFilterWhere(['like', 'contacts', $this->contacts]);
	    $query->andFilterWhere(['like', Status::tableName() . '.name', $this->statusName]);
	    if($this->lastFileName && mb_strpos("Отсутствует",$this->lastFileName,0,"UTF8")!==false){
		    $query->andWhere(['or',
			    ['like', File::tableName() . '.filename', $this->lastFileName],
			    ['lastFileId' => null]]);
	    }
	    else{
		    $query->andFilterWhere(['like', File::tableName().'.filename', $this->lastFileName]);
	    }
	    $query->andFilterWhere(['like', 'CONCAT(' .
		    User::tableName().'.surname,' .
		    "' '," .
		    User::tableName().'.name' .
		    ')', $this->expertFullname]);


	    return $dataProvider;
    }
}
