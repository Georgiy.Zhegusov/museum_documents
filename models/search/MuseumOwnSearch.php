<?php

namespace app\models\search;

use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Status;
use app\models\User;
use app\models\Museum;
use app\models\File;
use yii\db\Expression;

/**
 * EventSearch represents the model behind the search form about `app\models\Event`.
 */
class MuseumOwnSearch extends MuseumSearch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer', 'message'=>'Поле Уровень доступа должно быть целым числом'],
	        [['name', 'statusName', 'expertFullname', 'comment', 'lastFileName'], 'safe'],
            [['name'], 'string', 'max' => 256, 'message'=>'Длинна поля {attribute} ограничена 256 символами'],
	        [['comment'], 'string', 'message'=>'Поле {attribute} должно быть строкой'],
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'statusName' => 'Статус',
			'expertFullname' => 'Эксперт',
			'lastFileName' => 'Крайний файл',
		];
	}

	public $statusName;
	public $expertFullname;
	public $lastFileName;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
	    $dataProvider = parent::search($params);

	    /** @var User $user */
        $user = Yii::$app->user->identity;

        if($user->group->role == 'expert') {
            $dataProvider->query->andWhere([User::tableName() . '.id' => Yii::$app->user->id]);
        }

        if($user->group->role == 'depcult'){
            $dataProvider->query->andWhere(['!=', Museum::tableName() . '.statusId', 1]);
        }
	    return $dataProvider;
    }
}
