<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Museum;
use app\models\User;
use app\models\File;

/**
 * EventSearch represents the model behind the search form about `app\models\Event`.
 */
class FileSearch extends File
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer', 'message'=>'Поле Уровень доступа должно быть целым числом'],
	        [['filename','museumName','uploadedExpertName','created_at'], 'safe'],
        ];
    }

	public $museumName;
	public $uploadedExpertName;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id=null)
    {
        $query = File::find()->orderBy(['created_at'=>SORT_ASC])->filterWhere([Museum::tableName() . '.id' => $id]);
	    $query->joinWith(['museum']);
	    $query->joinWith(['uploadedExpert']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

	    $dataProvider->sort->attributes['museumName'] = [
		    // The tables are the ones our relation are configured to
		    // in my case they are prefixed with "tbl_"
		    'asc' => [Museum::tableName().'.name' => SORT_ASC],
		    'desc' => [Museum::tableName().'.name' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['uploadedExpertName'] = [
		    // The tables are the ones our relation are configured to
		    // in my case they are prefixed with "tbl_"
		    'asc' => [User::tableName().'.surname' => SORT_ASC, User::tableName().'.name' => SORT_ASC],
		    'desc' => [User::tableName().'.surname' => SORT_DESC, User::tableName().'.name' => SORT_DESC],
	    ];

	    if (!($this->load($params) && $this->validate())) {
		    return $dataProvider;
	    }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['like', 'filename', $this->filename]);
	    $query->andFilterWhere(['like', 'created_at', $this->created_at]);
	    $query->andFilterWhere(['like', Museum::tableName().'.name', $this->museumName]);
	    $query->andFilterWhere(['like', 'CONCAT(' .
		    User::tableName().'.surname,' .
		    "' '," .
		    User::tableName().'.name' .
		    ')', $this->uploadedExpertName]);

	    return $dataProvider;
    }
}
