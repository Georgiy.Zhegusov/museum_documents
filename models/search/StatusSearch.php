<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Status;

/**
 * UserSearch represents the model behind the search form about `app\models\Status`.
 */
class StatusSearch extends Status
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer', 'message'=>'Поле ID должно быть целым числом'],
            [['name', 'isLock'], 'safe'],
            [['isLock'], 'boolean', 'message'=>'Неверное значение'],
            [['name'], 'string', 'max' => 256, 'message'=>'Длинна поля {attribute} ограничена 256 символами'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
	    $query = Status::find();

	    // add conditions that should always apply here

	    $dataProvider = new ActiveDataProvider([
		    'query' => $query,
	    ]);

	    if (!($this->load($params) && $this->validate())) {
		    return $dataProvider;
	    }

	    // grid filtering conditions
	    $query->andFilterWhere([
		    'id' => $this->id,
		    'isLock' => $this->isLock,
	    ]);
	    $query->andFilterWhere(['like', 'name', $this->name]);

	    return $dataProvider;
    }
}
