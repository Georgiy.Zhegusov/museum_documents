<?php

namespace app\models;

use app\common\components\ActiveRecord\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "groups".
 *
 * @property integer $id
 * @property string $name
 * @property integer $level
 * @property string $role
 *
 * @property User[] $users
 */
class Group extends ActiveRecord
{

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level'], 'integer','message'=>'Поле Уровень доступа должно быть целым числом'],
            [['triggered'], 'boolean', 'message'=>'Неверное значение'],
            [['name', 'role'], 'string', 'max' => 64, 'message'=>'Длинна поля {attribute} ограничена 64 символами'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'level' => 'Уровень доступа',
            'role' => 'Роль',
            'triggered' => 'Генерирут события ?',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['groupId' => 'id']);
    }
}
