<?php

namespace app\models;

use yii;
use yii\web\UploadedFile;
use yii\behaviors\TimestampBehavior;
use app\common\components\ActiveRecord\ActiveRecord;


/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property string $filename
 * @property string $realPath
 * @property string $url
 * @property boolean $isLocked
 * @property integer $uploadedExpertId
 * @property integer $museumId
 * @property string $createdAt
 *
 * @property Museum $museum
 * @property User $uploadedExpert
 */
class File extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['museumId'], 'required','message'=>'Поле {attribute} необходимо для заполнения'],
            [['museumId'], 'integer','message'=>'Поле {attribute} должно быть числом'],
            [['museumId'], 'exist', 'skipOnError' => false, 'targetClass' => Museum::className(), 'targetAttribute' => ['museumId' => 'id'],'message'=>'Некорректное значение'],
	        ['url', 'url', 'defaultScheme' => 'http','message'=>'Некорректный адрес'],
	        [['filePole'], 'file', 'skipOnEmpty' => true, 'extensions' => 'docx, doc, rar, zip, pdf, xls, xlsx','wrongExtension'=>'Допустимы только файлы {extensions}'],
	        [['museumId'],'filter','filter'=>function($value){return $value==''?null:intval($value);}],
	        ['filePole', 'required','when' => function($model){
		          return is_null($model->url);
	            },
		        'whenClient' => "function (attribute, value) {
                    return false;
                }",
	            'message' => 'Файл или ссылка обязательны к заполнению.'],
	        ['filePole', 'compare','compareValue' => '', 'operator' => '=','when' => function($model){
		        return !is_null($model->url);
	        },
		        'whenClient' => "function (attribute, value) {
                    return false;
                }",
		        'message' => 'Должно быть заполнено что то одно.'],
	        ['url', 'required','when' => function($model){
		        return is_null($model->file);
	        },
		        'whenClient' => "function (attribute, value) {
                    return false;
                }",
		        'message' => 'Файл или ссылка обязательны к заполнению.'],
	        ['url', 'compare','compareValue' => '', 'operator' => '=','when' => function($model){
		        return !is_null($model->file);
	        },
		        'whenClient' => "function (attribute, value) {
                    return false;
                }",
		        'message' => 'Должно быть заполнено что то одно.'],
	        ['museumId', 'compare','compareValue' => $this->museumLastId, 'operator' => '==','when' => function($model){
			        return !is_null($model->museumLast);
		        },
		        'whenClient' => "function (attribute, value) {
                    return false;
                }",
		        'message' => 'Запрещена смена привязки музея для крайних файлов. Данный файл является крайним для ' .
			        $this->museumLastName],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Файл',
            'realPath' => 'Полный путь',
            'isLocked' => 'Заблокирован',
            'uploadedExpertId' => 'Автор',
            'museumId' => 'Музей',
            'created_at' => 'Загружен',
            'updated_at' => 'Обновлен',
	        'file' => 'Файл на загрузку',
	        'url' => 'URL',
	        'filePole' => 'Файл',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseum()
    {
        return $this->hasOne(Museum::className(), ['id' => 'museumId']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMuseumLast()
	{
		return $this->hasOne(Museum::className(), ['lastFileId' => 'id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedExpert()
    {
        return $this->hasOne(User::className(), ['id' => 'uploadedExpertId']);
    }

	public function getUploadedExpertName()
	{
		return $this->uploadedExpert->fullname;
	}

	public function getMuseumName()
	{
		return $this->museum->name;
	}

	public function getMuseumLastId(){
		if(is_null($this->museumLast)){
			return null;
		}
		return $this->museumLast->id;
	}

	public function getMuseumLastName(){
		if(is_null($this->museumLast)){
			return null;
		}
		return $this->museumLast->name;
	}

	public function getPath( )
	{
		return Yii::getAlias('@app').'/uploads/' . $this->realPath;
	}

	public function loadMuseum(Museum $museum)
	{
		$this->museumId = $museum->id;
	}

	public function loadFiles()
	{
		$this->file = UploadedFile::getInstance($this, 'filePole');
	}

	/**
	 * @var UploadedFile
	 */
	public $filePole;
	public $file;

	public function upload()
	{
		if(is_null($this->file)){
			$this->filename = $this->url;
			$this->realPath = null;
			return true;
		}
		if ($this->validate()) {
			$this->realPath = ($this->realPath?$this->realPath:rand(1000000,9999999) . rand(1000000,9999999));
			$this->filename = $this->file->name;
			$this->file->saveAs($this->getPath());
			return true;
		} else {
			return false;
		}
	}

	public function getIsLast(){
		return !is_null($this->museumLast);
	}

	public function beforeSave($insert)
	{
		if (!parent::beforeSave($insert)) {
			return false;
		}
		$this->uploadedExpertId = Yii::$app->user->id;
		return $this->upload();
	}

	public function afterSave($insert, $changedAttributes){
		parent::afterSave($insert, $changedAttributes);
		if($insert){
			$this->museum->link('lastFile',$this);
			$this->save();
		}
	}

	public function isWithFile(){
		return $this->url!='' || !is_null($this->file);
	}
}
