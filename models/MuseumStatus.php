<?php

namespace app\models;

use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "museum_statuses".
 *
 * @property integer $id
 * @property integer $status_id
 * @property integer $museum_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $created_by
 *
 * @property Museum $museum
 * @property Status $status
 */
class MuseumStatus extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'museum_statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id', 'museum_id', 'created_at', 'updated_at', 'updated_by', 'created_by'], 'integer'],
            [['museum_id'], 'exist', 'skipOnError' => true, 'targetClass' => Museum::className(), 'targetAttribute' => ['museum_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_id' => 'Status ID',
            'museum_id' => 'Museum ID',
            'created_at' => 'Время добавления',
            'updated_at' => 'Время изменения',
            'updated_by' => 'Обновил',
            'created_by' => 'Добавил',
	        'status.name' => 'Статус',
	        'museum.name' => 'Музей',
        ];
    }

	/**
	 * @return array
	 */
	public function behaviors() {
		return [
			BlameableBehavior::className(),
			TimestampBehavior::className(),
		];
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseum()
    {
        return $this->hasOne(Museum::className(), ['id' => 'museum_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

	/**
	 * @param integer|integer[] $status
	 * @param null|integer $user
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public static function getMStatusesByStatus($status,$userId=null){
    	$mStatuses = self::find()->where(['status_id'=>$status]);
		$mStatuses->andFilterWhere(['created_by'=>$userId]);
		return $mStatuses->all();
    }
}
