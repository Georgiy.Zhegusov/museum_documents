<?php

namespace app\models;

use yii;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use app\common\components\ActiveRecord\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $phone
 * @property string $authKey
 * @property string $password
 *
 * @property File[] $files
 * @property Museum[] $museums
 */
class User extends ActiveRecord
	implements
		IdentityInterface
{

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'phone', 'groupId'], 'required','message'=>'Поле {attribute} необходимо для заполнения'],
            [['name', 'surname', 'authKey'], 'string', 'max' => 64,'message'=>'Длинна имени ограничена 64 символами'],
            [['email'], 'string', 'max' => 128,'message'=>'Длинна почты ограничена 128 символами'],
            [['phone'], 'string', 'max' => 20,'message'=>'Длинна телефона ограничена 20 символами'],
            [['email'], 'unique','message'=>'Учетная запись с такой почтой уже зарегестрирована'],
	        [['groupId'], 'in', 'range' => ArrayHelper::getColumn(
		        Group::find()
			        ->where(['<=', 'level', Yii::$app->user->identity->group->level])
			        ->all(), 'id'),'message'=>'Вы не можете устанавливать группу выше себя или данной группы не существует'],
	        ['passwordFirst', 'compare','compareAttribute'=>'passwordSecond','operator'=>'==','message'=>'Введенные пароли должны совпадать'],
	        ['passwordSecond', 'compare','compareAttribute'=>'passwordFirst','operator'=>'==','message'=>'Введенные пароли должны совпадать'],
	        [['groupId'],'filter','filter'=>function($value){return $value==''?null:intval($value);}]
        ];
    }

	public $passwordFirst;
	public $passwordSecond;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'email' => 'E-mail',
            'phone' => 'Телефон',
	        'groupId' => 'Группа',
	        'groupName' => 'Группа',
            'authKey' => 'Auth Key',
            'password' => 'Пароль',
	        'passwordFirst' => 'Введите пароль( Только для его смены )',
	        'passwordSecond' => 'Повторите пароль',
	        'created_at' => 'Создан',
	        'updated_at' => 'Изменен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['uploadedExpertId' => 'id']);
    }

    public function getUsername()
    {
        return $this->surname . ' ' . $this->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseums()
    {
        return $this->hasMany(Museum::className(), ['expertId' => 'id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup()
	{
		return $this->hasOne(Group::className(), ['id' => 'groupId']);
	}   

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if( $insert || $this->passwordFirst!='' ) {
				$this->password = $this->decrypt($this->passwordFirst);
				$this->generateAuthKey();
			}
			return true;
		} else {
			return false;
		}
	}

	public function generateAuthKey()
	{
		$this->authKey = Yii::$app->security->generateRandomString();
	}

	public function getGroupName()
	{
		return $this->group->name;
	}

    public function getFullname()
    {
        return $this->surname . ' ' . $this->name;
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public static function findByUsername($usename)
    {
        return static::findOne(['email' => $usename]);
    }

    public function validatePassword($pass)
    {
		return $this->decrypt($pass)==$this->password;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

	private function decrypt( $string ){
		return crypt($string, "$5\$rounds=132000\$".$this->email);
	}

}
