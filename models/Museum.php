<?php

namespace app\models;

use yii;
use yii\helpers\ArrayHelper;
use app\common\components\ActiveRecord\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "museums".
 *
 * @property integer $id
 * @property string $name
 * @property integer $statusId
 * @property integer $expertId
 * @property string $comment
 * @property string $contacts
 * @property File $lastFileId
 * @property integer $additional
 *
 * @property File[] $files
 * @property Status $status
 * @property MuseumStatus $lastMStatus
 * @property MuseumStatus[] $mStatuses
 * @property User $expert
 */
class Museum extends ActiveRecord
{

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'museums';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required','message'=>'Поле {attribute} необходимо для заполнения'],
            [['statusId', 'expertId', 'additional'], 'integer','message'=>'Поле {attribute} должно быть целым числом'],
            [['comment'], 'string','message'=>'Комментарий должен быть строкой'],
            [['name'], 'string', 'max' => 255, 'message'=>'Длинна имени ограничена 255 символами'],
            [['contacts'], 'string', 'max' => 512, 'message'=>'Длинна имени ограничена 512 символами'],
            [['statusId'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['statusId' => 'id'], 'message'=>'Некорретный статус'],
            [['expertId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['expertId' => 'id'], 'message'=>'Некорректный эксперт'],
	        [['lastFileId'], 'exist', 'skipOnError' => false, 'targetClass' => File::className(), 'targetAttribute' => ['lastFileId' => 'id'], 'message'=>'Некорректный файл'],
	        [['statusId', 'expertId','lastFileId'],'filter','filter'=>function($value){return $value==''?null:intval($value);}]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'statusId' => 'Статус',
            'expertId' => 'Эксперт',
            'comment' => 'Комментарий',
            'contacts' => 'Контакты',
            'lastFileId' => 'Крайний файл',
	        'created_at' => 'Создано',
	        'updated_at' => 'Обновлено',
	        'additional' => 'Доп. инфо',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['museumId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastFile()
    {
        return $this->hasOne(File::className(), ['id' => 'lastFileId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'statusId']);
    }

	/**
	 * @return \yii\db\ActiveRecord
	 */
	public function getLastMStatus()
	{
		return $this->getMStatuses()->orderBy('created_at')->one();
	}

	/**
	 * @return yii\db\ActiveQuery
	 */
	public function getMStatuses()
	{
		return $this->hasMany(MuseumStatus::className(), ['museum_id' => 'id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpert()
    {
	    return $this->hasOne(User::className(), ['id' => 'expertId']);
    }

    public function getLastFileName()
    {
        $lastFile = $this->lastFile;

        return $lastFile ? $lastFile->filename : '';
    }

    public function getStatusName()
    {
        $status = $this->status;

        return $status ? $status->name : '';
    }

    public function getAdditionalName()
    {
        return ArrayHelper::getValue(self::getAdditionalList(), $this->additional, null);
    }

    public static function getAdditionalList()
    {
        return [
            1 => 'Дошкольники',
            2 => 'ОВЗ',
            3 => 'Дошкольники + ОВЗ'
        ];
    }

    public static function getStatusList()
    {
        return array_flip(ArrayHelper::getColumn(Status::find()->orderBy('name')->all(),'name'));
    }

	public function afterSave($insert, $changedAttributes)
	{
		if(isset($changedAttributes['statusId']) || $insert){
			$museumStatus = new MuseumStatus([
				'status_id' => $this->statusId,
				'museum_id' => $this->id
			]);
			$museumStatus->save();
		}
		parent::afterSave($insert, $changedAttributes);
	}
}
