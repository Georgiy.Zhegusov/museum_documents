<?php

namespace app\models;

use app\common\components\ActiveRecord\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "statuses".
 *
 * @property integer $id
 * @property string $name
 * @property boolean $isValuer
 * @property boolean $isLock
 *
 */
class Status extends ActiveRecord
{

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'isLock', 'isValuer'], 'required','message'=>'Поле {attribute} необходимо для заполнения'],
            [['isLock','isValuer', 'isDepCult'], 'boolean', 'message'=>'Неверное значение'],
            [['name'], 'string', 'max' => 256, 'message'=>'Длинна имени ограничена 256 символами'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'isLock' => 'Блокирующий ?',
	        'isValuer' => 'Оценивается ?',
            'isDepCult' => 'Департамента культуры'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['triggeredStatusId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseums()
    {
        return $this->hasMany(Museum::className(), ['statusId' => 'id']);
    }


    public static function getValueredStatuses(){
    	return self::find()->where(['isValuer'=>true])->all();
    }
}
