<?php

namespace app\models;

use yii;
use yii\helpers\ArrayHelper;
use app\common\components\ActiveRecord\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $name
 * @property integer $triggeredEvent
 */
class Event extends ActiveRecord
{

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','triggeredEvent'], 'required','message'=>'Поле {attribute} необходимо для заполнения'],
            [['name','triggeredEvent'], 'string', 'max' => 256, 'message'=>'Длинна имени ограничена 256 символами'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'triggeredEvent' => 'Генерируемое событие',
        ];
    }
}
