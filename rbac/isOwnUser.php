<?php
namespace app\rbac;

use Yii;
use yii\rbac\Rule;
use yii\web\ForbiddenHttpException;

class isOwnUser extends Rule
{

	public $name = 'isOwnUser';

	public function execute($user, $item, $params)
	{
		//throw new ForbiddenHttpException('Accept blocked for '.$user.$params['user']);

		//return true;
		return (isset($params['user']) && $user == $params['user']->id);
	}

}