<?php
namespace app\rbac;

use Yii;
use yii\rbac\Rule;
use yii\web\ForbiddenHttpException;

class isLastFile extends Rule
{

	public $name = 'isLastFile';

	public function execute($user, $item, $params)
	{
		//throw new ForbiddenHttpException('Accept blocked for '.$user.$params['user']);

		//return true;
		return (isset($params['file']) && $params['file']->isLast);
	}

}