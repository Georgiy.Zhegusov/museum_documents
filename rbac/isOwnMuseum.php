<?php
namespace app\rbac;

use Yii;
use yii\rbac\Rule;
use yii\web\ForbiddenHttpException;

class isOwnMuseum extends Rule
{

	public $name = 'isOwnMuseum';

	public function execute($user, $item, $params)
	{
		//throw new ForbiddenHttpException('Accept blocked for '.$user.$params['user']);

		//return true;
		return (
			isset($params['museum']) &&
			is_object($params['museum']) &&
			isset($params['museum']->expert->id) &&
			isset($params['museum']->status->isLock) &&
			$user == $params['museum']->expert->id &&
			$params['museum']->status->isLock == false
		);
	}

}