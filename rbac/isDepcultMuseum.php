<?php
namespace app\rbac;

use Yii;
use yii\rbac\Rule;
use yii\web\ForbiddenHttpException;

class isDepcultMuseum extends Rule
{

	public $name = 'isStartedMuseum';

	public function execute($user, $item, $params)
	{
		//throw new ForbiddenHttpException('Accept blocked for '.$user.$params['user']);

        //return true;
        return (
            isset($params['museum']) &&
            is_object($params['museum']) &&
            $params['museum']->status->isDepCult
        );
	}

}