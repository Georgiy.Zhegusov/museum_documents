<?php

namespace app\modules\logger;
use yii\base\Widget;
use app\modules\logger\models\Log;

class loggerWidget extends Widget
{
	public $object;
	public $userId;
	public $userClass;
	public $byUserOnly;
	private $logs;

	public function init()
	{
		parent::init();
		$conditions = [
			'object' => $this->object,
			'byUserOnly' => $this->byUserOnly];

		if(is_array($this->userId)){
			$conditions['userId'] = $this->userId;
		}

		$this->logs = Log::findFor($conditions);

		$this->userClass = logger::getInstance()->userClass;
	}

	public function run()
	{
		parent::run();

		return $this->render('widgetIndex',[
			'logs' => $this->logs,
			'userClass' => $this->userClass,
		]);

	}
}