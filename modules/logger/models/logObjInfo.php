<?php

namespace app\modules\logger\models;

use yii;

/**
 * This is the model class for table "loggerObjects".
 *
 * @property integer $id
 * @property string $objectClass
 * @property integer $objectId
 *
 * @property Log[] $Log
 */
class logObjInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loggerObjects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['objectId'], 'integer'],
            [['objectClass'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'objectClass' => 'Object Class',
            'objectId' => 'Object ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoggerLogs()
    {
        return $this->hasMany(Log::className(), ['id' => 'loggerLogsId'])->viaTable('loggerLogs_loggerObjects', ['loggerObjectsId' => 'id']);
    }

    public static function exists($conditions=[]){
        return self::find()->where($conditions)->exists();
    }
}
