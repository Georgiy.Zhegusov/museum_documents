<?php

namespace app\controllers;

use yii;
use app\models\File;
use app\models\search\FileSearch;
use app\models\Museum;
use app\models\AutoStatus;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\events\FileLoadEvent;


/**
 * FileController implements the CRUD actions for File model.
 */
class FileController extends Controller
{

	const FILE_UPLOAD = 'fileUpload';
	const FILE_DOWNLOAD = 'fileDownload';
	const FILE_DEPCULT_UPLOAD = 'fileDepcultUpload';
	const FILE_DEPCULT_DOWNLOAD = 'fileDepcultDownload';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function afterAction($action, $result)
	{
		// Проверить, что данный Action не зарегестрирован в событиях и не меняет статус
		return parent::afterAction($action, $result);
	}

	/**
     * Lists all File models.
     * @return mixed
     */
    public function actionIndex()
    {
	    if(!Yii::$app->user->can('fileIndex')){
		    return $this->redirect(['museum/index'], 302);
	    }

	    $searchModel = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	        'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	public function actionIndex_for_museum($id=null)
	{
		if(!Yii::$app->user->can('fileIndex')){
			return $this->redirect(['index'], 302);
		}
		if(is_null($id) || is_null($museum = Museum::findOne($id))){
			return $this->redirect(['index'], 302);
		}
		$searchModel = new FileSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}


	/**
     * Displays a single File model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	    if(!Yii::$app->user->can('fileView')){
		    return $this->redirect(['index'], 302);
	    }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new File model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	    if(!Yii::$app->user->can('fileCreate')){
		    return $this->redirect(['index'], 302);
	    }

	    $model = new File();
	    $model->loadFiles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        if(Yii::$app->user->identity->group->triggered) {
	            if(Yii::$app->user->identity->group->role == 'depcult'){
                    AutoStatus::setTriggeredStatus(self::FILE_DEPCULT_UPLOAD, $model->museum);
                }
                else{
                    AutoStatus::setTriggeredStatus(self::FILE_UPLOAD, $model->museum);
                }
	        }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

	public function actionCreate_for_museum($id=null)
	{
		$museum = Museum::findOne($id);

		if(!Yii::$app->user->can('fileCreate',['museum'=>$museum])){
			return $this->redirect(['create'], 302);
		}

		if(is_null($id) || is_null($museum)){
			return $this->redirect(['create'], 302);
		}

		$model = new File();
		$model->loadFiles();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			if(Yii::$app->user->identity->group->triggered) {
                if(Yii::$app->user->identity->group->role == 'depcult'){
                    AutoStatus::setTriggeredStatus(self::FILE_DEPCULT_UPLOAD, $model->museum);
                }
                else{
                    AutoStatus::setTriggeredStatus(self::FILE_UPLOAD, $model->museum);
                }
			}
			return $this->redirect(['museum/index']);
		} else {
			$model->loadMuseum( $museum );
			return $this->render('create_for_museum', [
				'model' => $model,
			]);
		}
	}

    /**
     * Updates an existing File model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	    if(!Yii::$app->user->can('fileUpdate')){
		    return $this->redirect(['index'], 302);
	    }

        $model = $this->findModel($id);
	    $model->loadFiles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing File model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	    if(!Yii::$app->user->can('fileDelete')){
		    return $this->redirect(['index'], 302);
	    }

        $file = $this->findModel($id);

	    if(is_null($file->museumLast))
	    {
		    $file->delete();
	    }
	    return $this->redirect(['index']);
    }

	public function actionDownload($id)
	{
		$model = $this->findModel($id);

		if(is_null($model) || !Yii::$app->user->can('fileDownload',[
				'museum'=>$model->museum,
				'file'=>$model])){
			return $this->redirect(['index'], 302);
		}

		if(Yii::$app->user->identity->group->triggered) {
            if(Yii::$app->user->identity->group->role == 'depcult'){
                AutoStatus::setTriggeredStatus(self::FILE_DEPCULT_DOWNLOAD, $model->museum);
            }
            else{
                AutoStatus::setTriggeredStatus(self::FILE_DOWNLOAD, $model->museum);
            }
		}

		Yii::$app->response->sendFile($model->path,$model->filename);
	}

    /**
     * Finds the File model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return File the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = File::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
