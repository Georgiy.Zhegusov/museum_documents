<?php

namespace app\controllers;

use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Museum;

use app\models\MuseumStatus;

/**
 * UserController implements the CRUD actions for User model.
 */
class MigrateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionMove(){
	    $museums = Museum::find()->all();
	    foreach ($museums as $museum) {
		    $museumStatus = new MuseumStatus([
			    'status_id' => $museum->statusId,
			    'museum_id' => $museum->id
		    ]);
		    $museumStatus->save();
	    }
	    return 'all Ok';
    }

}
