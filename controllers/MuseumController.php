<?php

namespace app\controllers;

use yii;
use yii\web\Response;
use app\models\Status;
use app\models\search\MuseumSearch;
use app\models\search\MuseumOwnSearch;
use app\models\File;
use app\models\Museum;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MuseumController implements the CRUD actions for Museum model.
 */
class MuseumController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Museum models.
     * @return mixed
     */
    public function actionIndex()
    {
	    if(!Yii::$app->user->can('museumIndex')){
		    return $this->redirect(['expert'], 302);
	    }

        $searchModel = new MuseumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	        'statuses' => Status::find()->indexBy('id')->orderBy('id')->all(),
        ]);
    }

	/**
	 * Lists all Museum models for expert.
	 * @return mixed
	 */
	public function actionExpert()
	{
		if(!Yii::$app->user->can('museumExpert')){
			return $this->redirect(['site/login'], 302);
		}

		$searchModel = new MuseumOwnSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('expert', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

    /**
     * Displays a single Museum model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	    $museum = $this->findModel($id);

	    if(!Yii::$app->user->can('museumView', ['museum' => $museum])){
		    return $this->redirect(['index'], 302);
	    }

        return $this->render('view', [
            'model' => $museum,
        ]);
    }

    /**
     * Creates a new Museum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	    if(!Yii::$app->user->can('museumCreate')){
		    return $this->redirect(['index'], 302);
	    }

        $model = new Museum();
		$file = new File();
	    $file->loadFiles();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        if($file->load(Yii::$app->request->post()) && $file->isWithFile()){
		        $file->museumId = $model->id;
		        if($file->save()){
			        return $this->redirect(['view', 'id' => $model->id]);
		        }
		        else{
			        return $this->render('create', [
				        'model' => $model,
				        'statuses' => Status::find()->indexBy('id')->orderBy('id')->all(),
				        'file' => $file,
			        ]);
		        }
	        }
	        else{
		        return $this->redirect(['view', 'id' => $model->id]);
	        }
        } else {
            return $this->render('create', [
                'model' => $model,
                'statuses' => Status::find()->indexBy('id')->orderBy('id')->all(),
	            'file' => $file,
            ]);
        }
    }

    /**
     * Updates an existing Museum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

	    if(!Yii::$app->user->can('museumUpdate')){
		    return $this->redirect(['index'], 302);
	    }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'statuses' => Status::find()->indexBy('id')->orderBy('id')->all(),
            ]);
        }
    }

	public function actionAjaxUpdate( ){
		if(!Yii::$app->user->can('museumUpdate')){
			\Yii::$app->response->format = Response::FORMAT_JSON;
			return ['output' => '', 'message' => 'Недостаточно прав'];
		}

		if (isset($_POST['hasEditable'])) {

			\Yii::$app->response->format = Response::FORMAT_JSON;

			if(!isset(Yii::$app->request->post('Museum')['id'])){
				return ['output' => '', 'message' => ''];
			}
/*
			ob_start();
			var_dump(Yii::$app->request->post('Museum'));
			return ['output' => '', 'message' => ob_get_clean()];*/

			$museumId = Yii::$app->request->post('Museum')['id'];

			/** @var Museum $museum */
			$museum = Museum::findOne(['id'=>$museumId]);
			$museum->load(Yii::$app->request->post());
			if( $museum->save() ) {
				$value = $museum->statusName;
				return ['output' => $value, 'message' => ''];
			}
			else{
				$errors = $museum->getFirstErrors();
				$string = 'Ошибки:';
				foreach ( $errors as $attribute => $error){
					$string.= "$attribute - $error; ";
				}
				return ['output' => '', 'message' => $string];
			}
		}

		return true;
	}

    /**
     * Deletes an existing Museum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	    if(!Yii::$app->user->can('museumDelete')){
		    return $this->redirect(['index'], 302);
	    }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Museum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Museum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Museum::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
