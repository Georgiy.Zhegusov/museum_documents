<?php

namespace app\controllers;

use app\models\Status;
use Yii;
use app\models\User;
use app\models\search\UserSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MuseumStatus;
use app\models\Museum;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
	    if(!Yii::$app->user->can('userIndex')){
		    return  $this->redirect(['museum/index'], 302);
	    }

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	        'findValuedMuseumsCount' => [$this,'getValuedMuseums']
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	    if(!Yii::$app->user->can('userView',['user'=>Yii::$app->user])){
		    return $this->redirect(['index'], 302);
	    }

        return $this->render('view', [
            'model' => $this->findModel($id),
	        'valuedMuseums' => $this->getValuedMuseums($this->findModel($id),false),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	    if(!Yii::$app->user->can('userCreate')){
		    return $this->redirect(['index'], 302);
	    }

        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	    if(!Yii::$app->user->can('userUpdate',['user'=>Yii::$app->user])){
		    return $this->redirect(['view','id'=>$id], 302);
	    }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	    if(!Yii::$app->user->can('userDelete')){
		    return $this->redirect(['index'], 302);
	    }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	/**
	 * @param User $user
	 * @param bool $countOnly
	 * @return mixed
	 */
	public function getValuedMuseums($user, $countOnly=true){
		$valuerStatuses = ArrayHelper::getColumn(
			Status::getValueredStatuses(),
			function($element){
				return $element->id;
			}
		);

	    $mStatuses = MuseumStatus::find()
		    ->where([
			    'created_by'=>$user->id,
			    'status_id'=>$valuerStatuses])
		    ->all();

	    $museumIds = ArrayHelper::getColumn($mStatuses,function($element){
		    return $element->museum_id;
	    });

		if($countOnly) {
			return Museum::find()->where(['id' => $museumIds])->count();
		}
		else{
			return Museum::find()->where(['id' => $museumIds]);
		}
    }
}
