# noinspection SqlNoDataSourceInspectionForFile

CREATE DATABASE IF NOT EXISTS museum_doc_dev DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER ON museum_doc_dev.* TO museum_doc_dev@localhost IDENTIFIED BY 'EjvPg2V3';