<?php
namespace app\commands;

use app\rbac\isDepcultMuseum;
use app\rbac\isStartedMuseum;
use yii;
use yii\console\Controller;
use app\rbac\isOwnMuseum;
use app\rbac\isOwnUser;
use app\rbac\isLastFile;


class RbacController extends Controller
{
	public function actionInit()
	{
		$auth =  Yii::$app->authManager;
		// clear all
		$auth->removeAll();

	////Museum block
		// add "museuNav" permission
		$museumNav = $auth->createPermission('museumNav');
		$museumNav->description = 'Create museum menu in Nav';
		$auth->add($museumNav);

		// add "museumCreate" permission
		$museumCreate = $auth->createPermission('museumCreate');
		$museumCreate->description = 'Create a museum';
		$auth->add($museumCreate);

		// add "museumIndex" permission
		$museumIndex = $auth->createPermission('museumIndex');
		$museumIndex->description = 'index a museum';
		$auth->add($museumIndex);

		// add "museumExpert" permission
		$museumExpert = $auth->createPermission('museumExpert');
		$museumExpert->description = 'own index a museum';
		$auth->add($museumExpert);

		// add "museumView" permission
		$museumView = $auth->createPermission('museumView');
		$museumView->description = 'view a museum';
		$auth->add($museumView);

		// add "museumUpdate" permission
		$museumUpdate = $auth->createPermission('museumUpdate');
		$museumUpdate->description = 'Update a museum';
		$auth->add($museumUpdate);

		// add "museumDelete" permission
		$museumDelete = $auth->createPermission('museumDelete');
		$museumDelete->description = 'delete a museum';
		$auth->add($museumDelete);

		// add "isOwnMuseum" rule
		$museumIsOwnRule = new isOwnMuseum();
		$auth->add($museumIsOwnRule);

		// add "isDepcultMuseum" rule
		$museumIsDepcultRule = new isDepcultMuseum();
		$auth->add($museumIsDepcultRule);

		// user can view only own museum
		$museumViewOwn = $auth->createPermission('museumViewOwn');
		$museumViewOwn->description = 'view an own user';
		$museumViewOwn->ruleName = $museumIsOwnRule->name;
		$auth->add($museumViewOwn);
		$auth->addChild($museumViewOwn,$museumView);

        $museumIsViewInited = new isStartedMuseum();

		// user can view only own museum
		$museumViewInited = $auth->createPermission('museumViewInited');
        $museumViewInited->description = 'view all inited museums';
        $museumViewInited->ruleName = $museumIsViewInited->name;
		$auth->add($museumViewInited);
		$auth->addChild($museumViewInited,$museumView);
		
	////File block
		// add "fileNav" permission
		$fileNav = $auth->createPermission('fileNav');
		$fileNav->description = 'Create file menu in Nav';
		$auth->add($fileNav);

		// add "fileCreate" permission
		$fileCreate = $auth->createPermission('fileCreate');
		$fileCreate->description = 'Create a file';
		$auth->add($fileCreate);

		// add "fileIndex" permission
		$fileIndex = $auth->createPermission('fileIndex');
		$fileIndex->description = 'index a file';
		$auth->add($fileIndex);

		// add "fileView" permission
		$fileView = $auth->createPermission('fileView');
		$fileView->description = 'view a file';
		$auth->add($fileView);

		// add "fileUpdate" permission
		$fileUpdate = $auth->createPermission('fileUpdate');
		$fileUpdate->description = 'Update a file';
		$auth->add($fileUpdate);

		// add "fileDelete" permission
		$fileDelete = $auth->createPermission('fileDelete');
		$fileDelete->description = 'delete a file';
		$auth->add($fileDelete);

		// add "fileDownload" permission
		$fileDownload = $auth->createPermission('fileDownload');
		$fileDownload->description = 'delete a file';
		$auth->add($fileDownload);

		// add "isLastFile" rule
		$fileIsLastRule = new isLastFile();
		$auth->add($fileIsLastRule);

		// user can create only own file
		$fileCreate_for_own_museum = $auth->createPermission('fileCreate_for_own_museum');
		$fileCreate_for_own_museum->description = 'create only own museum';
		$fileCreate_for_own_museum->ruleName = $museumIsOwnRule->name;
		$auth->add($fileCreate_for_own_museum);
		$auth->addChild($fileCreate_for_own_museum,$fileCreate);

		// user can create only own file
		$fileCreate_for_depcult_museum = $auth->createPermission('fileCreate_for_depcult_museum');
        $fileCreate_for_depcult_museum->description = 'create only own museum';
        $fileCreate_for_depcult_museum->ruleName = $museumIsDepcultRule->name;
		$auth->add($fileCreate_for_depcult_museum);
		$auth->addChild($fileCreate_for_depcult_museum,$fileCreate);

        // user can create only own file
        $fileDownload_for_depcult_museum = $auth->createPermission('fileDownload_for_depcult_museum');
        $fileDownload_for_depcult_museum->description = 'create only own museum';
        $fileDownload_for_depcult_museum->ruleName = $museumIsDepcultRule->name;
        $auth->add($fileDownload_for_depcult_museum);
        $auth->addChild($fileDownload_for_depcult_museum,$fileDownload);

        // user can download only own museum's last file
        $fileDownload_for_depcult_last_museum = $auth->createPermission('fileDownload_for_depcult_last_museum');
        $fileDownload_for_depcult_last_museum->description = 'download only own last museum';
        $fileDownload_for_depcult_last_museum->ruleName = $fileIsLastRule->name;
        $auth->add($fileDownload_for_depcult_last_museum);
        $auth->addChild($fileDownload_for_depcult_last_museum,$fileDownload_for_depcult_museum);

		// user can download only own museum's file
		$fileDownload_for_own_museum = $auth->createPermission('fileDownload_for_own_museum');
		$fileDownload_for_own_museum->description = 'download only own museum';
		$fileDownload_for_own_museum->ruleName = $museumIsOwnRule->name;
		$auth->add($fileDownload_for_own_museum);
		$auth->addChild($fileDownload_for_own_museum,$fileDownload);

		// user can download only own museum's last file
		$fileDownload_for_own_last_museum = $auth->createPermission('fileDownload_for_own_last_museum');
		$fileDownload_for_own_last_museum->description = 'download only own last museum';
		$fileDownload_for_own_last_museum->ruleName = $fileIsLastRule->name;
		$auth->add($fileDownload_for_own_last_museum);
		$auth->addChild($fileDownload_for_own_last_museum,$fileDownload_for_own_museum);

	////User block
		// add "userNav" permission
		$userNav = $auth->createPermission('userNav');
		$userNav->description = 'Create user menu in Nav';
		$auth->add($userNav);

		// add "userCreate" permission
		$userCreate = $auth->createPermission('userCreate');
		$userCreate->description = 'Create a user';
		$auth->add($userCreate);

		// add "userIndex" permission
		$userIndex = $auth->createPermission('userIndex');
		$userIndex->description = 'index a user';
		$auth->add($userIndex);

		// add "userView" permission
		$userView = $auth->createPermission('userView');
		$userView->description = 'view a user';
		$auth->add($userView);

		// add "userUpdate" permission
		$userUpdate = $auth->createPermission('userUpdate');
		$userUpdate->description = 'Update a user';
		$auth->add($userUpdate);

		// add "userDelete" permission
		$userDelete = $auth->createPermission('userDelete');
		$userDelete->description = 'delete a user';
		$auth->add($userDelete);

		// add "isOwnUser" rule
		$userIsOwnRule = new isOwnUser();
		$auth->add($userIsOwnRule);

		// user can view only own user
		$userViewOwn = $auth->createPermission('userViewOwn');
		$userViewOwn->description = 'view an own user';
		$userViewOwn->ruleName = $userIsOwnRule->name;
		$auth->add($userViewOwn);
		$auth->addChild($userViewOwn,$userView);

		// user can update only own user
		$userUpdateOwn = $auth->createPermission('userUpdateOwn');
		$userUpdateOwn->description = 'update an own user';
		$userUpdateOwn->ruleName = $userIsOwnRule->name;
		$auth->add($userUpdateOwn);
		$auth->addChild($userUpdateOwn,$userUpdate);

	////Status block
		// add "statusNav" permission
		$statusNav = $auth->createPermission('statusNav');
		$statusNav->description = 'Create status menu in Nav';
		$auth->add($statusNav);

		// add "statusCreate" permission
		$statusCRUD = $auth->createPermission('statusCRUD');
		$statusCRUD->description = 'CRUD status';
		$auth->add($statusCRUD);

	////Status line
		// add "statusLineNav" permission
		$statusLineNav = $auth->createPermission('statusLineNav');
		$statusLineNav->description = 'Create statusLine menu in Nav';
		$auth->add($statusLineNav);

		// add "statusLineView" permission
		$statusLineView = $auth->createPermission('statusLineView');
		$statusLineView->description = 'view status Line';
		$auth->add($statusLineView);
		
	////Status history
		// add "statusHistoryNav" permission
		$statusHistoryNav = $auth->createPermission('statusHistoryNav');
		$statusHistoryNav->description = 'Create statusHistory menu in Nav';
		$auth->add($statusHistoryNav);

		// add "statusHistoryView" permission
		$statusHistory = $auth->createPermission('statusHistory');
		$statusHistory->description = 'view status Line';
		$auth->add($statusHistory);

	////AutoStatus block
		// add "autoStatusNav" permission
		$autoStatusNav = $auth->createPermission('autoStatusNav');
		$autoStatusNav->description = 'Create autoStatus menu in Nav';
		$auth->add($autoStatusNav);

		// add "autoStatusCRUD" permission
		$autoStatusCRUD = $auth->createPermission('autoStatusCRUD');
		$autoStatusCRUD->description = 'CRUD autoStatus';
		$auth->add($autoStatusCRUD);

	////Event block
		// add "eventNav" permission
		$eventNav = $auth->createPermission('eventNav');
		$eventNav->description = 'Create event menu in Nav';
		$auth->add($eventNav);

		// add "eventCreate" permission
		$eventCRUD = $auth->createPermission('eventCRUD');
		$eventCRUD->description = 'CRUD event';
		$auth->add($eventCRUD);

	////Group block
		// add "groupNav" permission
		$groupNav = $auth->createPermission('groupNav');
		$groupNav->description = 'Create group menu in Nav';
		$auth->add($groupNav);

		// add "groupCreate" permission
		$groupCRUD = $auth->createPermission('groupCRUD');
		$groupCRUD->description = 'CRUD group';
		$auth->add($groupCRUD);

	////Log block
		// add "museumLog" permission
		$museumLog = $auth->createPermission('museumLog');
		$museumLog->description = 'see museum logs';
		$auth->add($museumLog);

	////History block
		// add "museumHistory" permission
		$museumHistory = $auth->createPermission('museumHistory');
		$museumHistory->description = 'see museumHistory';
		$auth->add($museumHistory);

	// create roles
		$guest  = $auth->createRole('guest');
		$expert = $auth->createRole('expert');
		$depcult = $auth->createRole('depcult');
		$admin = $auth->createRole('admin');
		$root = $auth->createRole('root');

	//register roles in a system
		$auth->add($guest);
		$auth->add($expert);
		$auth->add($admin);
		$auth->add($depcult);
		$auth->add($root);

	//Add rules for guest

	//Add guest rules to expert
		$auth->addChild($expert,$guest);

	//Add rules for expert
		//Add museum
		$auth->addChild($expert,$museumNav);
		$auth->addChild($expert,$museumExpert);
		$auth->addChild($expert,$museumViewOwn);

		//Add file
		$auth->addChild($expert,$fileCreate_for_own_museum);
		$auth->addChild($expert,$fileDownload_for_own_last_museum);

		//Add user
		$auth->addChild($expert,$userViewOwn);

		//Add user
		$auth->addChild($expert,$userUpdateOwn);

    //Add rules for depcult
        //Add museum

        $auth->addChild($depcult,$museumNav);
        $auth->addChild($depcult,$museumExpert);
        $auth->addChild($depcult,$museumViewInited);
        //Add file
        $auth->addChild($depcult,$fileCreate_for_depcult_museum);
        $auth->addChild($depcult,$fileDownload_for_depcult_last_museum);

        //Add user
        $auth->addChild($depcult,$userViewOwn);

        //Add user
        $auth->addChild($depcult,$userUpdateOwn);

	//Add expert rules to admin
		$auth->addChild($admin,$expert);
	//Add rules for admin
		//Add museum
		$auth->addChild($admin,$museumCreate);
		$auth->addChild($admin,$museumUpdate);
		$auth->addChild($admin,$museumDelete);
		$auth->addChild($admin,$museumIndex);
		$auth->addChild($admin,$museumView);

		//Add file
		$auth->addChild($admin,$fileNav);
		$auth->addChild($admin,$fileCreate);
		$auth->addChild($admin,$fileUpdate);
		$auth->addChild($admin,$fileIndex);
		$auth->addChild($admin,$fileView);
		$auth->addChild($admin,$fileDownload);

		//Add status
		$auth->addChild($admin,$statusNav);
		$auth->addChild($admin,$statusCRUD);

		//Add statusLine
		$auth->addChild($admin, $statusLineNav);
		$auth->addChild($admin, $statusLineView);

		//Add autoStatus
		$auth->addChild($root,$autoStatusNav);
		$auth->addChild($root,$autoStatusCRUD);

		//Add user
		$auth->addChild($admin,$userNav);
		$auth->addChild($admin,$userCreate);
		$auth->addChild($admin,$userUpdate);
		$auth->addChild($admin,$userDelete);
		$auth->addChild($admin,$userIndex);
		$auth->addChild($admin,$userView);

		//Add log
		$auth->addChild($admin,$museumLog);

		//Add history
		$auth->addChild($admin,$museumHistory);

	//Add admin rules to root
		$auth->addChild($root,$admin);
	//Add rules for root

		//Add files
		$auth->addChild($root,$fileDelete);
		//Add event
		$auth->addChild($root,$eventNav);
		$auth->addChild($root,$eventCRUD);

		//Add group
		$auth->addChild($root,$groupNav);
		$auth->addChild($root,$groupCRUD);
	}
}