<?php

use yii\db\Migration;

class m160614_231609_add_defaultRole_and_root_user extends Migration
{
    public function up()
    {
        $this->insert('groups', [
	        'id' => 1,
            'name' => 'Всемогущий',
            'level' => '4',
            'role' => 'root',
            'triggered' => 'false',
        ]);

	    $this->insert('groups', [
		    'id' => 2,
		    'name' => 'Администратор',
		    'level' => '3',
		    'role' => 'admin',
		    'triggered' => 'false',
	    ]);

	    $this->insert('groups', [
		    'id' => 3,
		    'name' => 'Эксперт',
		    'level' => '1',
		    'role' => 'expert',
		    'triggered' => 'false',
	    ]);

	    $this->insert('users', [
		    'id' => 1,
		    'name' => 'Георгий',
		    'surname' => 'Жегусов',
		    'email' => 'gaur-@mail.ru',
		    'phone' => '89031867393',
		    'authKey' => 'CqDnVMGWs4N8uBqQU0Op1O5ER2NkiSkY',
		    'password' => crypt('123456789', "$5\$rounds=132000\$gaur-@mail.ru"),
		    'groupId' => 1,
	    ]);
    }

    public function down()
    {
	    $this->delete('users', ['id' => 1]);
	    $this->delete('groups', ['id' => 3]);
	    $this->delete('groups', ['id' => 2]);
	    $this->delete('groups', ['id' => 1]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
