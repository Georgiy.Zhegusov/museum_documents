<?php

use yii\db\Migration;

/**
 * Handles the creation for table `museum_statuses`.
 * Has foreign keys to the tables:
 *
 * - `status`
 */
class m160808_092747_create_museum_statuses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('museum_statuses', [
            'id' => $this->primaryKey(),
            'status_id' => $this->integer(),
	        'museum_id' => $this->integer(),
	        'created_at' => $this->integer(),
	        'updated_at' => $this->integer(),
	        'updated_by' => $this->integer(),
	        'created_by' => $this->integer(),
        ]);

        // creates index for column `status_id`
        $this->createIndex(
            'idx-museum_statuses-status_id',
            'museum_statuses',
            'status_id'
        );

        // add foreign key for table `status`
        $this->addForeignKey(
            'fk-museum_statuses-status_id',
            'museum_statuses',
            'status_id',
            'statuses',
            'id',
            'RESTRICT',
            'CASCADE'
        );

	    // creates index for column `museum_id`
	    $this->createIndex(
		    'idx-museum_statuses-museum_id',
		    'museum_statuses',
		    'museum_id'
	    );

	    // add foreign key for table `museum`
	    $this->addForeignKey(
		    'fk-museum_statuses-museum_id',
		    'museum_statuses',
		    'museum_id',
		    'museums',
		    'id',
	        'RESTRICT',
		    'CASCADE'
	    );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `status`
        $this->dropForeignKey(
            'fk-museum_statuses-status_id',
            'museum_statuses'
        );

        // drops index for column `status_id`
        $this->dropIndex(
            'idx-museum_statuses-status_id',
            'museum_statuses'
        );

        $this->dropTable('museum_statuses');
    }
}
