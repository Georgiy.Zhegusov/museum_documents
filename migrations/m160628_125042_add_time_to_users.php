<?php

use yii\db\Migration;

/**
 * Handles adding time to table `users`.
 */
class m160628_125042_add_time_to_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('users','created_at',$this->integer());
        $this->addColumn('users','updated_at',$this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('users','created_at');
        $this->dropColumn('users','updated_at');
    }
}
