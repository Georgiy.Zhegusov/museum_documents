<?php

use yii\db\Migration;

/**
 * Handles adding isDepcult to table `statuses`.
 */
class m170701_000307_add_isDepcult_column_to_statuses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('statuses', 'isDepCult', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('statuses', 'isDepCult');
    }
}
