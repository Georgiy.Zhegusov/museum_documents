<?php

use yii\db\Migration;

/**
 * Handles adding time to table `groups`.
 */
class m160628_125122_add_time_to_groups extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('groups','created_at',$this->integer());
        $this->addColumn('groups','updated_at',$this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('groups','created_at');
        $this->dropColumn('groups','updated_at');
    }
}
