<?php

use yii\db\Migration;

/**
 * Handles adding time to table `autostatuses`.
 */
class m160628_125057_add_time_to_autoStatuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('autoStatuses','created_at',$this->integer());
        $this->addColumn('autoStatuses','updated_at',$this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('autoStatuses','created_at');
        $this->dropColumn('autoStatuses','updated_at');
    }
}
