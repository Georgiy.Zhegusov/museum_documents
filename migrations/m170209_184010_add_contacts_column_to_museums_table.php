<?php

use yii\db\Migration;

/**
 * Handles adding contacts to table `museums`.
 */
class m170209_184010_add_contacts_column_to_museums_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museums', 'contacts', $this->string(512));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('museums', 'contacts');
    }
}
