<?php

use yii\db\Migration;
use app\models\Status;

/**
 * Handles adding isValuer to table `statuses`.
 */
class m160808_184650_add_isValuer_column_to_statuses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('statuses', 'isValuer', $this->boolean());

	    $statuses = Status::find()->all();
	    foreach ($statuses as $status) {
		    $status->isValuer = 0;
		    $status->save();
	    }

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('statuses', 'isValuer');
    }
}
