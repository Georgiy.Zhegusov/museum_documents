<?php

use yii\db\Migration;

/**
 * Handles adding time to table `events`.
 */
class m160628_125111_add_time_to_events extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('events','created_at',$this->integer());
        $this->addColumn('events','updated_at',$this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('events','created_at');
        $this->dropColumn('events','updated_at');
    }
}
