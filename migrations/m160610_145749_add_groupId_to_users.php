<?php

use yii\db\Migration;

/**
 * Handles adding groupId to table `users`.
 */
class m160610_145749_add_groupId_to_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'groupId', $this->integer());
        $this->addForeignKey('users_groupId_to_group_Id', 'users', 'groupId', 'groups', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
	    $this->dropForeignKey('users_groupId_to_group_Id', 'users');
        $this->dropColumn('users', 'groupId');
    }
}
