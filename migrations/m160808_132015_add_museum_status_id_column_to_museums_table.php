<?php

use yii\db\Migration;

/**
 * Handles adding museum_status_id to table `museums`.
 * Has foreign keys to the tables:
 *
 * - `museum_status`
 */
class m160808_132015_add_museum_status_id_column_to_museums_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museums', 'museumStatus_id', $this->integer());

        // creates index for column `museum_status_id`
        $this->createIndex(
            'idx-museums-museum_status_id',
            'museums',
            'museumStatus_id'
        );

        // add foreign key for table `museum_status`
        $this->addForeignKey(
            'fk-museums-museum_status_id',
            'museums',
            'museumStatus_id',
            'museum_statuses',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `museum_status`
        $this->dropForeignKey(
            'fk-museums-museum_status_id',
            'museums'
        );

        // drops index for column `museum_status_id`
        $this->dropIndex(
            'idx-museums-museum_status_id',
            'museums'
        );

        $this->dropColumn('museums', 'museumStatus_id');
    }
}
