<?php

use yii\db\Migration;

/**
 * Handles adding name to table `events`.
 */
class m160621_080454_add_name_to_events extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('events', 'triggeredEvent', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('events', 'triggeredEvent');
    }
}
