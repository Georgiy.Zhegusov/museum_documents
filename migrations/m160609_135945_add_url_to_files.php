<?php

use yii\db\Migration;

/**
 * Handles adding url to table `files`.
 */
class m160609_135945_add_url_to_files extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('files', 'url', $this->string(1024));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('files', 'url');
    }
}
