<?php

use yii\db\Migration;

/**
 * Handles the creation for table `groups`.
 */
class m160610_145342_create_groups extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('groups', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64),
	        'level' => $this->integer(),
	        'role' => $this->string(64),
            'triggered' => $this->boolean()->notNull()->defaultValue(false),
        ], 'ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('groups');
    }
}
