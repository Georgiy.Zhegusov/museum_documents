<?php

use yii\db\Migration;

/**
 * Handles adding time to table `museums`.
 */
class m160628_125033_add_time_to_museums extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('museums','created_at',$this->integer());
        $this->addColumn('museums','updated_at',$this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('museums','created_at');
        $this->dropColumn('museums','updated_at');
    }
}
