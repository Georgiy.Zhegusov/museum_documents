<?php

use yii\db\Migration;

/**
 * Handles the creation for table `files`.
 */
class m160603_080716_create_files extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'filename' => $this->string(512),
            'realPath' => $this->string(512),
            'isLocked' => $this->boolean()->defaultValue(false)->notNull(),
            'uploadedExpertId' => $this->integer(),
            'museumId' => $this->integer()->notNull(),
            'createdAt' => $this->dateTime()->notNull(),
        ], 'ENGINE=InnoDB');

        $this->addForeignKey('fk-filesUploadedExpertId-usersId', 'files', 'uploadedExpertId', 'users', 'id', 'RESTRICT', 'CASCADE' );
        $this->addForeignKey('fk-filesMuseumId-museumsId', 'files', 'museumId', 'museums', 'id', 'RESTRICT', 'CASCADE' );
	    $this->addForeignKey('fk-museums_lastFile-filesId', 'museums', 'lastFileId', 'files', 'id', 'RESTRICT', 'CASCADE' );
    }

    /*I
     * @inheritdoc
     */
    public function safeDown()
    {
	    $this->dropForeignKey('fk-museums_lastFile-filesId','museums');
        $this->dropForeignKey('fk-filesMuseumId-museumsId', 'files');
        $this->dropForeignKey('fk-filesUploadedExpertId-usersId', 'files');
        $this->dropTable('files');
    }
}
