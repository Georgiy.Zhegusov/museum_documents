<?php

use yii\db\Migration;

/**
 * Handles dropping createdAt_column from table `files_table`.
 */
class m160803_205643_drop_createdAt_column_from_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('files', 'createdAt');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('files', 'createdAt', $this->dateTime()->notNull());
    }
}
