<?php

use yii\db\Migration;

/**
 * Handles the creation for table `museums`.
 */
class m160603_074300_create_museums extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('museums', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'statusId' => $this->integer(),
            'expertId' => $this->integer(),
            'comment' => $this->text(),
            'lastFileId' => $this->integer(),
        ], 'ENGINE=InnoDB');

        $this->addForeignKey('fk-museums_statusId-statusesId', 'museums', 'statusId', 'statuses', 'id', 'RESTRICT', 'CASCADE' );
        $this->addForeignKey('fk-museums_expertId-usersId', 'museums', 'expertId', 'users', 'id', 'RESTRICT', 'CASCADE' );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-museums_expertId-usersId','museums');
        $this->dropForeignKey('fk-museums_statusId-statusesId','museums');
        $this->dropTable('museums');
    }
}
