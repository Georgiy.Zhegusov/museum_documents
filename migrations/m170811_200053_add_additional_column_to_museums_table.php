<?php

use yii\db\Migration;

/**
 * Handles adding additional to table `museums`.
 */
class m170811_200053_add_additional_column_to_museums_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museums', 'additional', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('museums', 'additional');
    }
}
