<?php

use yii\db\Migration;

/**
 * Handles adding time to table `statuses`.
 */
class m160628_125047_add_time_to_statuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('statuses','created_at',$this->integer());
        $this->addColumn('statuses','updated_at',$this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('statuses','created_at');
        $this->dropColumn('statuses','updated_at');
    }
}
