<?php

use yii\db\Migration;

/**
 * Handles the creation for table `events`.
 */
class m160603_073326_create_events extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('events', [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull(),
            'triggeredStatusId' => $this->integer(),
        ], 'ENGINE=InnoDB');

        $this->addForeignKey('fk-events_triggeredStatusId-statusesId', 'events', 'triggeredStatusId', 'statuses', 'id', 'RESTRICT', 'CASCADE' );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-events_triggeredStatusId-statusesId', 'events');
        $this->dropTable('events');
    }
}
