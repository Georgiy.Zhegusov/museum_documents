<?php

use yii\db\Migration;

/**
 * Handles the creation for table `autostatuses`.
 */
class m160621_080341_create_autoStatuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('autoStatuses', [
            'id' => $this->primaryKey(),
            'initStatusId' => $this->integer(),
            'initEventId' => $this->integer()->notNull(),
            'triggeredStatusId' => $this->integer()->notNull(),
            'isActive' => $this->boolean(),
        ], 'ENGINE=InnoDB');

	    $this->addForeignKey('fk-autoStatuses_initStatusId-statusesId', 'autoStatuses', 'initStatusId', 'statuses', 'id', 'RESTRICT', 'CASCADE' );
	    $this->addForeignKey('fk-autoStatuses_initEventId-statusesId', 'autoStatuses', 'initEventId', 'events', 'id', 'RESTRICT', 'CASCADE' );
	    $this->addForeignKey('fk-autoStatuses_triggeredStatusId-statusesId', 'autoStatuses', 'triggeredStatusId', 'statuses', 'id', 'RESTRICT', 'CASCADE' );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('autostatuses');

	    $this->dropForeignKey('fk-autoStatuses_initStatusId-statusesId', 'autoStatuses');
	    $this->dropForeignKey('fk-autoStatuses_initEventId-statusesId', 'autoStatuses');
	    $this->dropForeignKey('fk-autoStatuses_triggeredStatusId-statusesId', 'autoStatuses');
    }
}
