<?php

use yii\db\Migration;

/**
 * Handles the creation for table `loggerLogs_loggerObjects`.
 * Has foreign keys to the tables:
 *
 * - `loggerLogs`
 * - `loggerObjects`
 */
class m160626_203026_create_junction_loggerLogs_and_loggerObjects extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('loggerLogs_loggerObjects', [
            'loggerLogsId' => $this->integer(),
            'loggerObjectsId' => $this->integer(),
            'PRIMARY KEY(loggerLogsId, loggerObjectsId)',
        ]);

        // creates index for column `loggerLogsId`
        $this->createIndex(
            'idx-loggerLogs_loggerObjects-loggerLogsId',
            'loggerLogs_loggerObjects',
            'loggerLogsId'
        );

        // add foreign key for table `loggerLogs`
        $this->addForeignKey(
            'fk-loggerLogs_loggerObjects-loggerLogsId',
            'loggerLogs_loggerObjects',
            'loggerLogsId',
            'loggerLogs',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `loggerObjectsId`
        $this->createIndex(
            'idx-loggerLogs_loggerObjects-loggerObjectsId',
            'loggerLogs_loggerObjects',
            'loggerObjectsId'
        );

        // add foreign key for table `loggerObjects`
        $this->addForeignKey(
            'fk-loggerLogs_loggerObjects-loggerObjectsId',
            'loggerLogs_loggerObjects',
            'loggerObjectsId',
            'loggerObjects',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `loggerLogs`
       $this->dropForeignKey(
            'fk-loggerLogs_loggerObjects-loggerLogsId',
            'loggerLogs_loggerObjects'
        );

        // drops foreign key for table `loggerObjects`
        $this->dropForeignKey(
            'fk-loggerLogs_loggerObjects-loggerObjectsId',
            'loggerLogs_loggerObjects'
        );


        // drops index for column `loggerObjectsId`
        $this->dropIndex(
            'idx-loggerLogs_loggerObjects-loggerObjectsId',
            'loggerLogs_loggerObjects'
        );

	    // drops index for column `loggerLogsId`
	    $this->dropIndex(
		    'idx-loggerLogs_loggerObjects-loggerObjectsId',
		    'loggerLogs_loggerObjects'
	    );

        $this->dropTable('loggerLogs_loggerObjects');
    }
}
