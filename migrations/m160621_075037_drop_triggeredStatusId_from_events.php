<?php

use yii\db\Migration;

/**
 * Handles dropping triggeredStatusId from table `events`.
 */
class m160621_075037_drop_triggeredStatusId_from_events extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-events_triggeredStatusId-statusesId', 'events');
	    $this->dropColumn('events', 'triggeredStatusId');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
	    $this->addColumn('events', 'triggeredStatusId', $this->integer());
        $this->addForeignKey('fk-events_triggeredStatusId-statusesId', 'events', 'triggeredStatusId', 'statuses', 'id', 'RESTRICT', 'CASCADE' );
    }
}
