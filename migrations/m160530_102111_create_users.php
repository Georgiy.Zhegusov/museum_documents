<?php

use yii\db\Migration;

/**
 * Handles the creation for table `users`.
 */
class m160530_102111_create_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'surname' => $this->string(64)->notNull(),
            'email' => $this->string(128)->notNull()->unique(),
            'phone' => $this->string(20)->notNull(),
            'authKey' => $this->string(64)->notNull(),
            'password' => $this->string(256)->notNull(),
        ], 'ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
