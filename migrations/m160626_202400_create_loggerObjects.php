<?php

use yii\db\Migration;

/**
 * Handles the creation for table `loggerobjects`.
 */
class m160626_202400_create_loggerObjects extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('loggerObjects', [
            'id' => $this->primaryKey(),
            'objectClass' => $this->string(),
            'objectId' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('loggerObjects');
    }
}
