<?php

use yii\db\Migration;

/**
 * Handles the creation for table `statuses`.
 */
class m160603_071235_create_statuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('statuses', [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull(),
            'isLock' => $this->boolean()->notNull(),
        ], 'ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('statuses');
    }
}
