<?php

use yii\db\Migration;

/**
 * Handles adding time to table `files`.
 */
class m160628_125028_add_time_to_files extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('files','created_at',$this->integer());
        $this->addColumn('files','updated_at',$this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('files','created_at');
        $this->dropColumn('files','updated_at');
    }
}
