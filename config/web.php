<?php

$params = require(__DIR__ . '/params.php');

$devOrNothing = (YII_ENV_DEV?'dev':'');
$slashDevOrNothing = (YII_ENV_DEV?'/'.$devOrNothing:'');

$config = [
	'homeUrl' => $slashDevOrNothing . '/museum_doc',
    'id' => 'basic',
    'defaultRoute' => 'museum/index',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','debug','logger'],
    'components' => [
        'authManager' => [
            'class' => 'app\common\components\DbManager\DbManager',
        ],
        'request' => [
            'baseUrl' => $slashDevOrNothing . '/museum_doc',
            'cookieValidationKey' => 'p7bJs6ndBJ55udTFJznOhneGVYm9w6q1',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
	        'identityCookie' => [
	        	'name' => '_museum_doc' . $devOrNothing,
		        'path' => 'museum_doc/' . $slashDevOrNothing,
		        'httpOnly' => true
	        ]
        ],
	    'session' => [
		    'name' => 'museum_doc_SESSION' . $devOrNothing,
		    'savePath' => __DIR__ . '/../runtime/session/',
	    ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'assetManager' => [
						'basePath' => '@app/web/assets/',
						'baseUrl' => $slashDevOrNothing . '/museum_doc/assets/',
				],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db' . $devOrNothing . '.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
	    'i18n' => [
		    'translations' => [
			    'app' => [
				    'class' => 'yii\i18n\PhpMessageSource',
				    'basePath' => '@app/messages',
			    ],
			    'yii2mod.*' => [
				    'class' => 'yii\i18n\PhpMessageSource',
				    'basePath' => '@app/messages',
			    ],
		    ]
	    ],
    ],
	'modules' => [
		'debug' => [
			'class' => 'yii\debug\Module',
			'allowedIPs' => ['127.0.0.1']
		],
		'logger' => [
			'class' => 'app\modules\logger\logger',
			'userClass' => 'app\models\User',
			'ignoredAttributes' => [
				'created_at',
				'updated_at'
			]
		],
		'comment' => [
			'class' => 'yii2mod\comments\Module',
			'controllerMap' => [
				'comments' => 'yii2mod\comments\controllers\ManageController'
			],
		],
		'gridview' =>  [
			'class' => '\kartik\grid\Module'
		]
	],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
